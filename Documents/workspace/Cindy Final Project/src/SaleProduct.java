import java.text.DecimalFormat;

import javax.swing.JOptionPane;
/**
 * This is asks for the input and prints out the total items purchased with the price.
 * @author csp000us
 *
 */
public class SaleProduct {
	public SaleProduct(){
		DecimalFormat theFormat = new DecimalFormat("##.00");
		double SALESTAX = 0.05;
			
		int quantityscupcakes = Integer.parseInt(JOptionPane.showInputDialog(null,"Enter number of strawberry cupcakes purchased "));
		int quantityccupcakes = Integer.parseInt(JOptionPane.showInputDialog(null,"Enter number of cherry cupcakes purchased "));
		int quantitybcupcakes = Integer.parseInt(JOptionPane.showInputDialog(null,"Enter number of blueberry cupcakes purchased "));
		int quantitylcupcakes = Integer.parseInt(JOptionPane.showInputDialog(null,"Enter number of lemon cupcakes purchased "));
			
		int quantitycbeverages = Integer.parseInt(JOptionPane.showInputDialog(null, "Enter number of coffee purchased"));
		int quantitymbeverages = Integer.parseInt(JOptionPane.showInputDialog(null, "Enter number of milk purchased"));
		int quantityjbeverages = Integer.parseInt(JOptionPane.showInputDialog(null, "Enter number of juice purchased"));
		int quantitysbeverages = Integer.parseInt(JOptionPane.showInputDialog(null, "Enter number of soda purchased"));

		double cupcakeprice = 4.50;
		double priceCoffee = 2.50;
		double priceMilk = 1.50;
		double priceJuice = 1.50;
		double priceSoda = 1.00;
		double total = ((double)quantityscupcakes*cupcakeprice) + ((double)quantityccupcakes*cupcakeprice) + ((double)quantitybcupcakes*cupcakeprice) +((double)quantitylcupcakes*cupcakeprice) + ((double)quantitycbeverages*priceCoffee) + ((double)quantitymbeverages*priceMilk) + ((double)quantityjbeverages*priceJuice) + ((double)quantitysbeverages*priceSoda);
		double tax = total*SALESTAX;
		double finaltotal = total + tax;		
		JOptionPane.showMessageDialog(null, "Strawberry cupcakes:"+ quantityscupcakes +"\n"+"Cherry cupcakes:"+ quantityscupcakes +"\n"+ "Blueberry cupcakes:"+ quantityscupcakes +"\n"+ "Lemon cupcakes:"+ quantityscupcakes +"\n"+ 
					"Coffee:" + quantitycbeverages +"\n"+ "Milk:"+ quantitymbeverages +"\n"+ "Juice:"+ quantityjbeverages +"\n"+"Soda:"+ quantitysbeverages +"\n"+ ("SemiTotal: "+ theFormat.format(total)) + ("\n"+ "Tax (0.05): "+ theFormat.format(tax)) + "\n"+ ("The total of the purchase is "+ theFormat.format(finaltotal)));
		
	}

}
