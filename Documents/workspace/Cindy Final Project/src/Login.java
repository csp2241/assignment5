import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 * This class is to initiate the user to Register or Login
 * @author csp000us
 *
 */
public class Login extends JFrame implements ActionListener{
JButton jbtnRegistration = new JButton("REGISTER");
JButton jbtnSignIn = new JButton("SIGN IN");
	public Login() {
		jbtnRegistration.addActionListener(this);
		jbtnSignIn.addActionListener(this);
		
		add(jbtnRegistration);
		add(jbtnSignIn);
		
		setLayout(new FlowLayout());
		setVisible(true);
		pack();
		setLocationRelativeTo(null);
	}
	/**
	 * This initiates the Login program
	 * @param args
	 */
	public static void main(String[] args) {
		new Login();
	}
	
	/**
	 * This makes connects to the other class it will either lead to the Registration class where they make a new account
	 * or it continue to the Authentication process.
	 */
	@Override
	public void actionPerformed(ActionEvent click) {
		// TODO Auto-generated method stub
		 Object source = click.getSource();
		if( source == jbtnRegistration) {
			new Registration();
		}else if (source == jbtnSignIn ) { 
			new Authentication();
		}
	}
}
