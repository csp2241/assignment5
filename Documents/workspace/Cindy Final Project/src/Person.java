import java.util.ArrayList;

/**
 * This is the part that that makes the username and password.
 * @author csp000us
 *
 */
public class Person {
	private String uname;
	private String pwd;
	private static int count;
	
	/**
	 * This allows the user to see the username.
	 * @return
	 */
	public String getUname() {
		return uname;
	}
	/**
	 * This sets the username.
	 * @param uname
	 */
	public void setUname(String uname) {
		this.uname = uname;
	}
	/**
	 * THis allows the user to see the password.
	 * @return
	 */
	public String getPwd() {
		return pwd;
	}
	/**
	 * this allows the user to set the password.
	 * @param pwd
	 */
	public void setPwd(String pwd) {
		this.pwd = pwd;
	}
	/**
	 * This allows the user to see the username and password together.
	 */
	@Override
	public String toString() {
		return "Person [uname=" + uname + ", pwd=" + pwd + "]";
	}
	

}	