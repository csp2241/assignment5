import java.text.DecimalFormat;

import javax.swing.JOptionPane;
/**
 * This is the menu of the sales product. 
 * @author csp000us
 *
 */
public class Menu {
	String cupcakeflavor;
	String drinktype;
	/**
	 * Overload constructor.
	 */
	public Menu() {
		cupcakeflavor = "";
		drinktype = "";
	}
	
	/**
	 * This shows the flavor of the cupcakes
	 */
	public void setcupcakeflavor() {
		JOptionPane.showMessageDialog(null, "Strawberry       $4.50"+ "\n" + "Cherry            $4.50"+ "\n" + "Blueberry        $4.50" + "\n" + "Lemon            $4.50");
	
	}
	/**
	 * This represents the drinks in the menu
	 */
	public void setdrinktype() {
		JOptionPane.showMessageDialog(null, "Coffee         $1.50" + "\n" + "Milk            $1.50"+ "\n" + "Juice          $1.50" + "\n" + "Soda           $1.50");

	}
	
	
}
