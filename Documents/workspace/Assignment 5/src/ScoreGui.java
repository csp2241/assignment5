import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 * This class is makes the grade calculator for class CSC130.
 * @author csp000us
 *
 */
public class ScoreGui extends JFrame implements ActionListener{
		String gradeError = "Wrong Input";
		final int SIZE = 7;
		JButton jbtCalculate = new JButton("Calculate");
		//scores text fields
		JTextField []  jtxtScore = new JTextField[SIZE];
		JTextField []  jtxtWeight = new JTextField[SIZE];
		//JTextField jtxtName = new JTextField(20);
		JLabel [] jlblModuleName = new JLabel[SIZE];
		//JLabel jlblOutput = new JLabel("0");
		JPanel scorePanel = new JPanel();
		JPanel buttonPanel = new JPanel();
		

	  //use for scoreCalculator method
		double [] scores = new double[SIZE];
		double [] weights = new double[SIZE];
		private double gradeAverage;

		
		/**
		 * Construct the graphic user interface for score calculator (Basically the Labels) .
		 */
		public ScoreGui() {
			jlblModuleName[0] = new JLabel("Student Name");
			jlblModuleName[1] = new JLabel("Assignments");
			jlblModuleName[2] = new JLabel("Midterm Exam");
			jlblModuleName[3] = new JLabel("Final Project");
			jlblModuleName[4] = new JLabel("Final Exam");
			jlblModuleName[5] = new JLabel("Grade Average");
			jlblModuleName[6] = new JLabel("Letter Grade");
			
			
			
			//Score panel 
			scorePanel.setBackground(Color.cyan);
			//This is to set the grid (rows, columns)
			scorePanel.setLayout(new GridLayout(7,3));
			//this allows the button to be shown
			
			for (int i=0; i<SIZE; i++) {
				scorePanel.add (jlblModuleName[i]);
				jtxtScore[i] = new JTextField(0);
				scorePanel.add(jtxtScore[i]);
				jtxtWeight[i] = new JTextField(0);
				scorePanel.add(jtxtWeight[i]);
	
					}
			//Button panel button
			buttonPanel.setBackground(Color.green);
			jbtCalculate.addActionListener(this);
			buttonPanel.add(jbtCalculate);
			add(scorePanel, BorderLayout.CENTER);
			add(buttonPanel, BorderLayout.SOUTH);
 
			//This allows one to see the JFrame
			setVisible(true);
			// This keeps it from being folded
			pack();
			// This allows one to keep it in the middle
			setLocationRelativeTo(null);
		}
		/**
		 * Calculate grade average scores
		 * @param score an array of scores
		 * @param weight an array of weights 
		 */
		public double calculateScore(double [] scores, double [] weights) { 
			double grade[] = new double[4];
			double gradeAverage = 0.0;

			//logic to calculate
			
			grade[0] = (scores[1]/5)*weights[1];
			grade[1] = scores[2]*weights[2];
			grade[2] = scores[3]*weights[3];
			grade[3] = scores[4]*weights[4];
			gradeAverage = grade[0] + grade[1] + grade[2] + grade[3];
			return gradeAverage;
			}
				
		/**
		 * Calculate the letter grade 
		 * @param score grade average
		 * @return letter grade A,B,C,D,F
		 */
		public char calculateLetterGrade(double gradeAverage) {
			char letter ='N';
			//logic to find letter grade
			gradeAverage = calculateScore(scores,weights);
			if (gradeAverage >=90)
				letter ='A';
			else if ((gradeAverage >=80) && (gradeAverage < 90))
			letter ='B';
			else if ((gradeAverage >=70) && (gradeAverage < 80))
			letter ='C';
			else if ((gradeAverage >=60) && (gradeAverage < 70))
			letter ='D';
			else if((gradeAverage >=0.0) && (gradeAverage < 60))
			letter= 'F';
				
			return letter;
		}

		@Override
		public void actionPerformed(ActionEvent e) {

			scores[1] = Double.parseDouble(jtxtScore[1].getText());
			scores[2] = Double.parseDouble(jtxtScore[2].getText());
			scores[3] = Double.parseDouble(jtxtScore[3].getText());
			scores[4] = Double.parseDouble(jtxtScore[4].getText());
			weights[1] = Double.parseDouble(jtxtWeight[1].getText());
			weights[2] = Double.parseDouble(jtxtWeight[2].getText());
			weights[3] = Double.parseDouble(jtxtWeight[3].getText());
			weights[4] = Double.parseDouble(jtxtWeight[4].getText());
			
		jtxtScore[5].setText(calculateScore(scores,weights)+ "" );
		jtxtScore[6].setText(calculateLetterGrade(gradeAverage)+"");
		
		
		}
		
		
}
